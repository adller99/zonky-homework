import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { LoanService } from '../../../services/loan.service';
import { Subscription } from 'rxjs';
import { Loan } from '../../../services/loan.model';


@Component({
  selector: 'app-loans-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss'],
})
export class DetailComponent implements OnInit, OnDestroy {

  loanId: number;
  loanSubscription: Subscription;
  loanDetail: Loan;

  constructor(private route: ActivatedRoute, private loanService: LoanService) {
  }

  ngOnInit() {
    this.loanId = +this.route.snapshot.params.id;

    this.loanSubscription = this.loanService
      .getLoanSubscription()
      .subscribe(this.filterLoanById.bind(this));

    this.loanService.getAllLoans();
  }

  ngOnDestroy(): void {
    this.loanSubscription.unsubscribe();
  }

  private filterLoanById(data: Loan[]): void {
    this.loanDetail = data.find(loan => loan.id === this.loanId);
  }
}
