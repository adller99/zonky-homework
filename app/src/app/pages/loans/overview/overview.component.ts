import { Component, OnDestroy, OnInit } from '@angular/core';
import { LoanService } from '../../../services/loan.service';
import { Loan } from '../../../services/loan.model';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-overview',
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.scss']
})
export class OverviewComponent implements OnInit, OnDestroy {

  public loans: Loan[] = [];
  public loanSubscription: Subscription;
  public enableAverageAmount = false;

  constructor(private loanService: LoanService) {
    this.loanSubscription = this.loanService
      .getLoanSubscription()
      .subscribe(data => this.loans = data);
  }

  ngOnInit() {
    this.loanService.getAllLoans();
  }

  ngOnDestroy(): void {
    this.loanSubscription.unsubscribe();
  }
}
