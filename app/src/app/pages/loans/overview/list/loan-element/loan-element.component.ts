import { Component, Input, OnInit } from '@angular/core';
import { Loan } from '../../../../../services/loan.model';

@Component({
  selector: 'app-loan-element',
  templateUrl: './loan-element.component.html',
  styleUrls: ['./loan-element.component.scss']
})
export class LoanElementComponent implements OnInit {

  @Input()
  loanDetail: Loan;

  constructor() { }

  ngOnInit() {
  }

}
