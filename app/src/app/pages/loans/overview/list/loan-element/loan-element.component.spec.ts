import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoanElementComponent } from './loan-element.component';

describe('LoanElementComponent', () => {
  let component: LoanElementComponent;
  let fixture: ComponentFixture<LoanElementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoanElementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoanElementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
