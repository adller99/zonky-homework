import { Component, Input, OnInit } from '@angular/core';
import { Loan } from '../../../../services/loan.model';

@Component({
  selector: 'app-loans-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

  @Input()
  loans: Loan[] = [];

  constructor() { }

  ngOnInit() {
  }
}
