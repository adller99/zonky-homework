import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { LoanService } from '../../../../services/loan.service';
import { Rating } from '../../../../services/rating.model';
import { faPowerOff, IconDefinition } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-rating-filter',
  templateUrl: './rating-filter.component.html',
  styleUrls: ['./rating-filter.component.scss'],
})
export class RatingFilterComponent implements OnInit {

  @Output()
  public enableFilterEmitter: EventEmitter<boolean> = new EventEmitter();

  ratings: Rating[] = [];
  selectedRating: Rating;
  resetIcon: IconDefinition = faPowerOff;

  constructor(private loanService: LoanService) {
  }

  ngOnInit() {
    this.getRatings();
  }

  private getRatings(): void {
    this.loanService.getRatings()
      .subscribe(data => this.ratings = data);
  }

  public filterByRating(rating: Rating): void {
    this.selectedRating = rating;
    this.loanService.getLoansByRating(rating);
    this.enableFilterEmitter.emit(true);
  }

  public resetFilter(): void {
    this.selectedRating = null;
    this.loanService.getAllLoans();
    this.enableFilterEmitter.emit(false);
  }
}
