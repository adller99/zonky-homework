import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AverageAmountComponent } from './average-amount.component';

describe('AverageAmountComponent', () => {
  let component: AverageAmountComponent;
  let fixture: ComponentFixture<AverageAmountComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AverageAmountComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AverageAmountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
