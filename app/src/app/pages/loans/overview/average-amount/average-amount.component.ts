import { Component, OnDestroy, OnInit } from '@angular/core';
import { LoanService } from '../../../../services/loan.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-average-amount',
  templateUrl: './average-amount.component.html',
  styleUrls: ['./average-amount.component.scss'],
})
export class AverageAmountComponent implements OnInit, OnDestroy {

  averageLoanValue;
  loanSubscription: Subscription;

  constructor(private loanService: LoanService) {
  }

  ngOnInit() {
    this.calculateAverageLoanValue();
  }

  ngOnDestroy(): void {
    this.loanSubscription.unsubscribe();
  }

  private calculateAverageLoanValue(): void {
    this.loanSubscription = this.loanService.getLoanSubscription()
      .subscribe(data => {
          const loansSum = data
            .map(loan => loan.amount)
            .reduce((a, b) => a + b);

          this.averageLoanValue = Math.round(loansSum / data.length);
      });
  }
}
