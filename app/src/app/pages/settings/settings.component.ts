import { Component, OnInit } from '@angular/core';

import { LocalizationService } from '../../services/localization.service';
import { Language } from '../../languag.eum';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit {

  public readonly definedLanguages = [
    {language: Language.EN, title: 'English'},
    {language: Language.CS, title: 'Čeština'},
  ];
  private currentLanguage: string;

  constructor(private localizationService: LocalizationService) {
    this.getCurrentLanguage();
  }

  ngOnInit() {
  }

  private getCurrentLanguage(): void {
    this.currentLanguage = this.localizationService.getCurrentLanguage();
  }

  public onLanguageSelection(language: string): void {
    this.localizationService.changeCurrentLanguage(language);
  }
}
