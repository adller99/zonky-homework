import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'currencyFormatter'
})
export class CurrencyFormatterPipe implements PipeTransform {

  private static readonly currency = 'CZK';
  private static readonly formatRegex = /\B(?=(\d{3})+(?!\d))/g;

  transform(value: any, ...args: any[]): any {
    return value.toString()
      .replace(CurrencyFormatterPipe.formatRegex, ' ') + ' ' + CurrencyFormatterPipe.currency;
  }

}
