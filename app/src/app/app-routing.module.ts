import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NotFoundComponent } from './pages/not-found/not-found.component';
import { OverviewComponent } from './pages/loans/overview/overview.component';
import { SettingsComponent } from './pages/settings/settings.component';
import { DetailComponent } from './pages/loans/detail/detail.component';

const routes: Routes = [
  {
    path: '',
    component: OverviewComponent,
  },
  {
    path: 'loans/:id',
    component: DetailComponent
  },
  {
    path: 'settings',
    component: SettingsComponent,
  },
  {
    path: 'not-found',
    component: NotFoundComponent,
  },
  {
    path: '**',
    redirectTo: '/not-found',
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {
}
