import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, ReplaySubject } from 'rxjs';
import 'rxjs/add/operator/map';
import { Loan } from './loan.model';
import { Rating } from './rating.model';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class LoanService {
  private readonly baseUlr: string = environment.apiBaseUrl;
  private loansSubject: ReplaySubject<Loan[]> = new ReplaySubject();

  constructor(private httpClient: HttpClient) { }

  public getLoanSubscription(): Observable<Loan[]> {
    return this.loansSubject.asObservable();
  }

  public getAllLoans(): void {
    this.httpClient.get<Loan[]>(this.baseUlr + 'loans')
      .map(data => data.map(loan => new Loan().deserialize(loan)))
      .subscribe((data) => {
      this.loansSubject.next(data);
    });
  }

  public getLoansByRating(rating: Rating): void {
    this.httpClient.get<Loan[]>(this.baseUlr + 'loans/' + rating.title)
      .map(data => data.map(loan => new Loan().deserialize(loan)))
      .subscribe(data => this.loansSubject.next(data));
  }

  public getRatings(): Observable<Rating[]> {
    return this.httpClient.get<Rating[]>(this.baseUlr + 'loans/ratings')
      .map(data => data.map(rating => new Rating().deserialize(rating)));
  }
}
