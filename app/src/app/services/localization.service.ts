import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class LocalizationService {
  constructor(private translateService: TranslateService) { }

  public setDefaultLanguage(): void {
    const defaultLanguage: string = environment.defaultLanguage;

    this.translateService.setDefaultLang(defaultLanguage);
    this.translateService.use(defaultLanguage);
  }

  public getCurrentLanguage(): string {
    return this.translateService.currentLang;
  }

  public changeCurrentLanguage(language: string): void {
    this.translateService.use(language);
  }
}
