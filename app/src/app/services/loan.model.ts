import { Deserializable } from '../commons/deserializable';

export class Loan implements Deserializable {
  id: number;
  story: string;
  amount: number;
  name: string;
  rating: string
  interestRate: number;

  deserialize(input: any): this {
    Object.assign(this, input);

    return this;
  }
}
