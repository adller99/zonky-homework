import { Deserializable } from '../commons/deserializable';

export class Rating implements Deserializable{
  title: string;
  value: number;

  deserialize(input: any): this {
    this.title = input.ratingTitle;
    this.value = input.ratingValue;

    return this;
  }
}
