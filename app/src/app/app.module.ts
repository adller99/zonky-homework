import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { NotFoundComponent } from './pages/not-found/not-found.component';
import { OverviewComponent } from './pages/loans/overview/overview.component';
import { MenuComponent } from './components/menu/menu.component';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { LocalizationService } from './services/localization.service';
import { SettingsComponent } from './pages/settings/settings.component';
import { FormsModule } from '@angular/forms';
import { RatingFilterComponent } from './pages/loans/overview/rating-filter/rating-filter.component';
import { AverageAmountComponent } from './pages/loans/overview/average-amount/average-amount.component';
import { CurrencyFormatterPipe } from './pipes/currency-formatter.pipe';
import { ListComponent } from './pages/loans/overview/list/list.component';
import { DetailComponent } from './pages/loans/detail/detail.component';
import { BackNavigationComponent } from './components/back-navigation/back-navigation.component';
import { LoanElementComponent } from './pages/loans/overview/list/loan-element/loan-element.component';

@NgModule({
  declarations: [
    AppComponent,
    NotFoundComponent,
    OverviewComponent,
    MenuComponent,
    SettingsComponent,
    RatingFilterComponent,
    AverageAmountComponent,
    CurrencyFormatterPipe,
    ListComponent,
    DetailComponent,
    BackNavigationComponent,
    LoanElementComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FontAwesomeModule,
    FormsModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    })

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(private localizationService: LocalizationService) {
    localizationService.setDefaultLanguage();
  }

}

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}
