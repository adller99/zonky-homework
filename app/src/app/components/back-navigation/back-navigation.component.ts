import { Component, Input, OnInit } from '@angular/core';
import { faLongArrowAltLeft, IconDefinition } from '@fortawesome/free-solid-svg-icons';
import { Router } from '@angular/router';

@Component({
  selector: 'app-back-navigation',
  templateUrl: './back-navigation.component.html',
  styleUrls: ['./back-navigation.component.scss'],
})
export class BackNavigationComponent implements OnInit {

  @Input()
  backUrlCommands: string[] = [];

  backIcon: IconDefinition = faLongArrowAltLeft;

  constructor(private router: Router) { }

  ngOnInit() {
  }

  onBackClick(): void {
    this.router.navigate(this.backUrlCommands);
  }
}
