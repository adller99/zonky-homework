import { Language } from '../app/languag.eum';

export const environment = {
  production: true,
  defaultLanguage: Language.CS,
  apiBaseUrl: 'http://localhost:8080/'
};
