package com.zonkyhomework.demo.dto.converter;

import com.zonkyhomework.demo.dto.MarketplaceLoan;
import com.zonkyhomework.demo.persistance.entity.Loan;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import static com.shazam.shazamcrest.matcher.Matchers.sameBeanAs;
import static com.zonkyhomework.demo.DataGenerator.getLoanEntity1;
import static com.zonkyhomework.demo.DataGenerator.getMarketplaceLoan1;
import static org.junit.Assert.assertThat;

@RunWith(SpringRunner.class)
public class LoanConverterTest {

    private LoanConverter loanConverter;

    @Before
    public void setUp() {
        loanConverter = new LoanConverter();
    }


    @Test
    public void convertDtoToEntity() {
        final Loan expectedResult = getLoanEntity1();
        final MarketplaceLoan marketplaceLoan = getMarketplaceLoan1();

        final Loan result = loanConverter.convert(marketplaceLoan);

        assertThat(result, sameBeanAs(expectedResult));
    }
}
