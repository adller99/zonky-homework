package com.zonkyhomework.demo.service;

import com.zonkyhomework.demo.DataGenerator;
import com.zonkyhomework.demo.dto.Rating;
import com.zonkyhomework.demo.dto.converter.LoanConverter;
import com.zonkyhomework.demo.exception.ApiErrorException;
import com.zonkyhomework.demo.persistance.entity.Loan;
import com.zonkyhomework.demo.persistance.repository.LoanRepository;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import static com.shazam.shazamcrest.matcher.Matchers.sameBeanAs;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class LoanServiceTest {

    private static final String GENERAL_ERROR_MESSAGE = "An error occurred during processing the request";
    private static final String STATUS_CODE = "statusCode";

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    private static DataAccessException dataAccessException = new DataAccessException("...") {};

    private LoanService loanService;

    @MockBean
    private LoanRepository loanRepository;

    @MockBean
    private MarketplaceService marketplaceService;

    @Before
    public void setUp() {
        final LoanConverter loanConverter = new LoanConverter();
        MockitoAnnotations.initMocks(this);
        loanService = new LoanService(marketplaceService, loanRepository, loanConverter);
    }

    @Test
    public void getAllLoans() {
        //generate expected result
        final List<Loan> expectedResult = Arrays.asList(DataGenerator.getLoanEntity1(), DataGenerator.getLoanEntity2());

        //mock the result
        when(loanRepository.findAll()).thenReturn(expectedResult);

        //call the service
        final List<Loan> result = loanService.getAllLoans();

        //assert results
        assertThat(result, sameBeanAs(expectedResult));
    }

    @Test
    public void getAllLoans_RepositoryErrorThrowsCustomException() {
        //matchers for expected exception
        thrown.expect(ApiErrorException.class);
        thrown.expect(hasProperty(STATUS_CODE, is(HttpStatus.INTERNAL_SERVER_ERROR)));
        thrown.expectMessage(GENERAL_ERROR_MESSAGE);

        //mock the result
        when(loanRepository.findAll()).thenThrow(dataAccessException);

        //call the service
        loanService.getAllLoans();
    }

    @Test
    public void getAllByRating() {
        //generated expected result
        final List<Loan> expectedResult = Arrays.asList(DataGenerator.getLoanEntity2(), DataGenerator.getLoanEntity2());

        //mock the result
        when(loanService.getAllByRating(anyString())).thenReturn(expectedResult);

        //call the service
        final List<Loan> result = loanService.getAllByRating(anyString());

        //assert the results
        assertThat(result, sameBeanAs(expectedResult));
    }

    @Test
    public void getAllByRating_RepositoryErrorThrowsCustomException() {
        // matchers for expected exception
        thrown.expect(ApiErrorException.class);
        thrown.expect(hasProperty(STATUS_CODE, is(HttpStatus.INTERNAL_SERVER_ERROR)));
        thrown.expectMessage(GENERAL_ERROR_MESSAGE);

        //mock the result
        when(loanRepository.findByRating(anyString())).thenThrow(dataAccessException);

        //call the service
        loanService.getAllByRating(anyString());
    }

    @Test
    public void getRatings() {
        //generate expected result
        final List<Loan> returnedData = Arrays.asList(DataGenerator.getLoanEntity1(),
                DataGenerator.getLoanEntity2());
        final Set<Rating> expectedResult = new LinkedHashSet<>(Arrays.asList(DataGenerator.getRating1(),
                DataGenerator.getRating2()));

        //mock the result
        when(loanRepository.findAll()).thenReturn(returnedData);

        //call the service
        final Set<Rating> result = loanService.getRatings();

        //compare the results
        assertThat(result, sameBeanAs(expectedResult));
    }

    @Test
    public void getRatings_RepositoryErrorThrowsCustomException() {
        // matchers for expected exception
        thrown.expect(ApiErrorException.class);
        thrown.expect(hasProperty(STATUS_CODE, is(HttpStatus.INTERNAL_SERVER_ERROR)));
        thrown.expectMessage(GENERAL_ERROR_MESSAGE);

        //mock the result
        when(loanRepository.findAll()).thenThrow(dataAccessException);

        //call the service
        loanService.getRatings();
    }
}
