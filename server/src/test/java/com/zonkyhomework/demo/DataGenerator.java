package com.zonkyhomework.demo;

import com.zonkyhomework.demo.dto.MarketplaceLoan;
import com.zonkyhomework.demo.dto.Rating;
import com.zonkyhomework.demo.persistance.entity.Loan;

public class DataGenerator {

    public static Rating getRating1() {
        return new Rating("AAA", 0.053D);
    }

    public static Rating getRating2() {
        return new Rating("AB", 0.085D);
    }


    public static Loan getLoanEntity1() {
        final Loan loan = new Loan();
        loan.setId(45156L);
        loan.setName("Pujcka na vsechno");
        loan.setStory("Děkuji všem co investovali do mé půjčky. Už se těším jak dceři Elišce " +
                "ukážu náš nový domov");
        loan.setRating(getRating1().getRatingTitle());
        loan.setInterestRate(getRating1().getRatingValue());
        loan.setAmount(454600);

        return loan;
    }

    public static MarketplaceLoan getMarketplaceLoan1() {
        final MarketplaceLoan marketplaceLoan = new MarketplaceLoan();
        marketplaceLoan.setId(45156L);
        marketplaceLoan.setAmount(454600);
        marketplaceLoan.setInterestRate(0.053D);
        marketplaceLoan.setName("Pujcka na vsechno");
        marketplaceLoan.setRating("AAA");
        marketplaceLoan.setStory("Děkuji všem co investovali do mé půjčky. Už se těším jak dceři Elišce " +
                "ukážu náš nový domov");


        return marketplaceLoan;
    }

    public static Loan getLoanEntity2() {
        final Loan loan = new Loan();
        loan.setId(467L);
        loan.setName("Pujcka na vsechno");
        loan.setStory("Dobrý den vážení investoři,\n" +
                "děkuji za investování Vašich prostředků, věřím, že se Vám výhodně zúročí.\n" +
                "Jsem zaměstnána v pojišťovnictví - automobily, vlastním také 3 nemovitosti v Praze, které pronajímám. Já sama bydlím v nájemním bytě.\n" +
                "Přeji Vám hezké léto.");
        loan.setRating(getRating2().getRatingTitle());
        loan.setInterestRate(getRating2().getRatingValue());
        loan.setAmount(15000000);

        return loan;
    }

    public static MarketplaceLoan getMarketplaceLoan2() {
        final MarketplaceLoan marketplaceLoan = new MarketplaceLoan();
        marketplaceLoan.setId(467L);
        marketplaceLoan.setAmount(15000000);
        marketplaceLoan.setInterestRate(0.085D);
        marketplaceLoan.setName("Pujcka na vsechno");
        marketplaceLoan.setRating("AB");
        marketplaceLoan.setStory("Dobrý den vážení investoři,\n" +
                "děkuji za investování Vašich prostředků, věřím, že se Vám výhodně zúročí.\n" +
                "Jsem zaměstnána v pojišťovnictví - automobily, vlastním také 3 nemovitosti v Praze, které pronajímám. Já sama bydlím v nájemním bytě.\n" +
                "Přeji Vám hezké léto.");
        return marketplaceLoan;
    }

}
