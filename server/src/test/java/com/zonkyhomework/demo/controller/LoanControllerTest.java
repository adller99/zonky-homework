package com.zonkyhomework.demo.controller;

import com.google.gson.GsonBuilder;
import com.zonkyhomework.demo.DataGenerator;
import com.zonkyhomework.demo.dto.Rating;
import com.zonkyhomework.demo.persistance.entity.Loan;
import com.zonkyhomework.demo.service.LoanService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.*;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(value = LoanController.class, secure = false)
public class LoanControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private LoanService loanService;

    private GsonBuilder gsonBuilder;

    @Before
    public void setUp() {
        gsonBuilder = new GsonBuilder();
    }

    @Test
    public void getAllNotesReturnsData() throws Exception {
        //generate expected data
        final List<Loan> expectedResult =
                Arrays.asList(DataGenerator.getLoanEntity1(), DataGenerator.getLoanEntity2());
        final String jsonExpectedResult = gsonBuilder.create().toJson(expectedResult);

        //mock the result
        when(loanService.getAllLoans()).thenReturn(expectedResult);

        //call the mocked API and assert status, content-type and length of retrieved json
        final MvcResult result = mockMvc.perform(get("/loans")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(expectedResult.size())))
                .andReturn();

        //assert the results
        assertThat(jsonExpectedResult, equalTo(result.getResponse().getContentAsString()));
    }

    // tests 204 status
    @Test
    public void getAllNotesReturns204() throws Exception {
        //mock the result
        when(loanService.getAllLoans()).thenReturn(new ArrayList<>());

        //call the mocked API and assert status, content-type
        final MvcResult result = mockMvc.perform(get("/loans")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent())
                .andReturn();

        assertThat("", equalTo(result.getResponse().getContentAsString()));
    }

    @Test
    public void getRatingsReturnsData() throws Exception {
        //generated expected data
        final Set<Rating> expectedResult =
                new LinkedHashSet<>(Arrays.asList(DataGenerator.getRating1(), DataGenerator.getRating2()));
        final String jsonExpectedRatings = gsonBuilder.create().toJson(expectedResult);

        //mock the result
        when(loanService.getRatings()).thenReturn(expectedResult);

        //call the mocked API and assert status, content-type and length of retrieved json
        final MvcResult result = mockMvc.perform(get("/loans/ratings")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(expectedResult.size())))
                .andReturn();

        //assert the results
        assertThat(jsonExpectedRatings, equalTo(result.getResponse().getContentAsString()));
    }

    // tests 204 status
    @Test
    public void getRatingsReturns204() throws Exception {
        //mock the result
        when(loanService.getRatings()).thenReturn(new LinkedHashSet<>());

        //call the mocked API and assert status, content-type
        final MvcResult result = mockMvc.perform(get("/loans/ratings")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent())
                .andReturn();

        assertThat("", equalTo(result.getResponse().getContentAsString()));
    }

    @Test
    public void getLoansByRatingReturnsData() throws Exception {
        //generated expected data
        final List<Loan> expectedResult = Collections.singletonList(DataGenerator.getLoanEntity1());
        final String rating = expectedResult.get(0).getRating();
        final String jsonExpectedLoans = gsonBuilder.create().toJson(expectedResult);

        //mock the result
        when(loanService.getAllByRating(rating)).thenReturn(expectedResult);

        //call the mocked API
        final MvcResult result = mockMvc.perform(get("/loans/" + rating)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(expectedResult.size())))
                .andReturn();

        //assert the results
        assertThat(jsonExpectedLoans, equalTo(result.getResponse().getContentAsString()));
    }

   // tests 204 status
    @Test
    public void getLoansByRatingReturns204() throws Exception {
        //generated expected data
        final String rating = DataGenerator.getRating1().getRatingTitle();

        //mock the result
        when(loanService.getAllByRating(rating)).thenReturn(new ArrayList<>());

        //call the mocked API and assert status, content-type
        final MvcResult result = mockMvc.perform(get("/loans/" + rating)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent())
                .andReturn();

        //assert the results
        assertThat("", equalTo(result.getResponse().getContentAsString()));
    }
}
