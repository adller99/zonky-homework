package com.zonkyhomework.demo.dto.converter;

import com.zonkyhomework.demo.dto.MarketplaceLoan;
import com.zonkyhomework.demo.persistance.entity.Loan;
import org.springframework.stereotype.Component;

/**
 * This class handles converting MarketPlace {@link MarketplaceLoan} to Loan {@link Loan} object
 */
@Component
public class LoanConverter {

    /**
     * This methods converts marketplace dto object to Loan object
     * @param marketplaceLoan source object of the conversion {@link MarketplaceLoan}
     * @return converted marketplaceLoan to Loan {@link Loan}
     */
    public Loan convert(MarketplaceLoan marketplaceLoan) {
        final Loan loan = new Loan();
        loan.setId(marketplaceLoan.getId());
        loan.setAmount(marketplaceLoan.getAmount());
        loan.setName(marketplaceLoan.getName());
        loan.setRating(marketplaceLoan.getRating().toUpperCase());
        loan.setStory(marketplaceLoan.getStory());
        loan.setInterestRate(marketplaceLoan.getInterestRate());

        return loan;
    }
}
