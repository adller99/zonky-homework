package com.zonkyhomework.demo.dto;

import java.util.Objects;

public class Rating {
    private String ratingTitle;
    private double ratingValue;

    public Rating(String ratingTitle, double ratingValue) {
        this.ratingTitle = ratingTitle;
        this.ratingValue = ratingValue;
    }

    public String getRatingTitle() {
        return ratingTitle;
    }

    public void setRatingTitle(String ratingTitle) {
        this.ratingTitle = ratingTitle;
    }

    public double getRatingValue() {
        return ratingValue;
    }

    public void setRatingValue(double ratingValue) {
        this.ratingValue = ratingValue;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Rating rating = (Rating) o;
        return ratingTitle.equals(rating.ratingTitle);
    }

    @Override
    public int hashCode() {
        return Objects.hash(ratingTitle);
    }
}
