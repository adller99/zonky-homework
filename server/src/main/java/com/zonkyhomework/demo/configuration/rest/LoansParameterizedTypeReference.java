package com.zonkyhomework.demo.configuration.rest;

import com.zonkyhomework.demo.dto.MarketplaceLoan;
import org.springframework.core.ParameterizedTypeReference;

import java.util.List;

public class LoansParameterizedTypeReference extends ParameterizedTypeReference<List<MarketplaceLoan>> {
}
