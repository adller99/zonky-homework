package com.zonkyhomework.demo.configuration.rest;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.DefaultUriBuilderFactory;

import java.time.Duration;
import java.util.Collections;

@Configuration
public class ZonkyRestTemplateConfiguration {

    @Value("${zonky.api.baseUrl}")
    private String BASE_URL;

    private static final int CONNECTION_TIMEOUT = 1; //minutes

    private final RestTemplateBuilder restTemplateBuilder;

    public ZonkyRestTemplateConfiguration(RestTemplateBuilder restTemplateBuilder) {
        this.restTemplateBuilder = restTemplateBuilder;
    }

    @Bean(name = "zonkyRestTemplate")
    public RestTemplate zonkyRestTemplate() {
        return restTemplateBuilder
                .uriTemplateHandler(new DefaultUriBuilderFactory(BASE_URL))
                .interceptors(Collections.singletonList(getZonkyHttpRequestInterceptor()))
                .setConnectTimeout(Duration.ofMinutes(CONNECTION_TIMEOUT))
                .setReadTimeout(Duration.ofMinutes(CONNECTION_TIMEOUT))
                .build();
    }

    @Bean
    public ZonkyHttpRequestInterceptor getZonkyHttpRequestInterceptor() {
        return new ZonkyHttpRequestInterceptor();
    }
}
