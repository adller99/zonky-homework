package com.zonkyhomework.demo.configuration.rest;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpRequest;
import org.springframework.http.MediaType;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;

import java.io.IOException;

/**
 * This interceptor adds header values
 */
public class ZonkyHttpRequestInterceptor implements ClientHttpRequestInterceptor {

    private static final String X_SIZE_HEADER = "X-size";
    public static final int RECORDS_PER_PAGE = 50;
    public static final String X_PAGE = "X-Page";
    private static final String USER_AGENT = "ZonkyHomework 1.1";

    @Override
    public ClientHttpResponse intercept(final HttpRequest request,final byte[] body,
                                        final ClientHttpRequestExecution execution)
            throws IOException {
        request.getHeaders().add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
        request.getHeaders().add(X_SIZE_HEADER, String.valueOf(RECORDS_PER_PAGE));
        request.getHeaders().add(HttpHeaders.USER_AGENT, USER_AGENT);

        return execution.execute(request, body);
    }
}
