package com.zonkyhomework.demo.configuration.security;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.web.cors.CorsConfiguration;


/**
 * This class extends springboot security configuration {@link WebSecurityConfiguration} and allows to add custom rules.
 */
@Configuration
@EnableWebSecurity
public class WebSecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Value("${zonky.core.corsEnabled}")
    private boolean corsEnabled;

    private static final String H2_MATCHER = "/h2-console/**";
    private static final String PERMIT_ALL_MATCHER = "/";

    /**
     * This method allows CORS for the development purposes
     */
    @Override
    protected void configure(HttpSecurity httpSecurity) throws Exception {
        if (corsEnabled) {
            final CorsConfiguration corsConfiguration = new CorsConfiguration().applyPermitDefaultValues();

            httpSecurity
                    .authorizeRequests()
                    .antMatchers(PERMIT_ALL_MATCHER).permitAll().and()
                    .authorizeRequests().antMatchers(H2_MATCHER).permitAll();
            httpSecurity.csrf().disable();
            httpSecurity.headers().frameOptions().disable();
            httpSecurity.cors().configurationSource(request -> corsConfiguration);
        }
    }
}
