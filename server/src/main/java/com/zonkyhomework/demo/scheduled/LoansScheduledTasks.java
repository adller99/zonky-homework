package com.zonkyhomework.demo.scheduled;

import com.zonkyhomework.demo.service.LoanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

/**
 * This class is responsible for scheduling a task that's fetching and storing all records from the Zonky's API
 */
@Configuration
@EnableScheduling
public class LoansScheduledTasks {

    private final LoanService loanService;

    @Autowired
    public LoansScheduledTasks(LoanService loanService) {
        this.loanService = loanService;
    }

    @Scheduled(fixedDelay = 300000, initialDelay = 0)
    public void getAndStoreLoans() {
        loanService.fetchAndStoreLoans();
    }
}
