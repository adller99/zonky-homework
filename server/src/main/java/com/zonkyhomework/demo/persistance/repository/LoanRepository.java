package com.zonkyhomework.demo.persistance.repository;

import com.zonkyhomework.demo.persistance.entity.Loan;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LoanRepository extends CrudRepository<Loan, Long> {

    /**
     * Retrieves all loans from the database
     * @return list of notes {@link List<Loan>}
     */
    List<Loan> findAll();

    /**
     * Retrieves loans based on provided rating
     * @return list of notes {@link List<Loan>}
     */
    List<Loan> findByRating(String rating);

}
