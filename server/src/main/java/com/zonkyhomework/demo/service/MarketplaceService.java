package com.zonkyhomework.demo.service;

import com.zonkyhomework.demo.configuration.rest.LoansParameterizedTypeReference;
import com.zonkyhomework.demo.dto.MarketplaceLoan;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.ArrayList;
import java.util.List;

import static com.zonkyhomework.demo.configuration.rest.ZonkyEndpoints.LOANS_MARKETPLACE;
import static com.zonkyhomework.demo.configuration.rest.ZonkyHttpRequestInterceptor.RECORDS_PER_PAGE;
import static com.zonkyhomework.demo.configuration.rest.ZonkyHttpRequestInterceptor.X_PAGE;

@Service
public class MarketplaceService {

    private static final Logger logger = LoggerFactory.getLogger(MarketplaceService.class);

    private final RestTemplate restTemplate;

    private final LoansParameterizedTypeReference loansParameterizedTypeReference = new LoansParameterizedTypeReference();

    private final UriComponentsBuilder uriBuilder = UriComponentsBuilder
            .fromUriString(LOANS_MARKETPLACE)
            .queryParam("fields", "id,name,story,rating,amount,interestRate")
            .queryParam("nonReservedRemainingInvestment__gt", "0");

    @Autowired
    public MarketplaceService(@Qualifier("zonkyRestTemplate") RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    /**
     * Fetches all active loans from the Zonky API
     *
     * @return the collection of marketplace loans {@link List<MarketplaceLoan>}
     */
    public List<MarketplaceLoan> fetchAllActiveLoans() {
        logger.debug("Started fetching records");
        int page = 0;
        boolean hasNext = true;
        final List<MarketplaceLoan> results = new ArrayList<>();

        while (hasNext) {
            logger.debug("Fetching page:{}", page + 1);
            try {
                final ResponseEntity<List<MarketplaceLoan>> responseEntity = fetchRecords(page);

                if (responseEntity.getBody() != null && responseEntity.getBody().size() != 0) {
                    final int count = responseEntity.getBody().size();
                    if (count != RECORDS_PER_PAGE) {
                        hasNext = false;
                    }

                    page++;
                    results.addAll(responseEntity.getBody());

                    logger.debug("Fetched records count:{}", count);
                    logger.debug("Total records count:{}", results.size());
                } else {
                    logger.debug("No records were fetched from the API");
                    break;
                }
            } catch (RestClientException exception) {
                logger.error("API Call ended with an error", exception);
                break;
            }
        }
        logger.debug("Finished fetching records");

        return results;
    }

    private ResponseEntity<List<MarketplaceLoan>> fetchRecords(int page) {
        final HttpHeaders headers = new HttpHeaders();
        headers.add(X_PAGE, String.valueOf(page));

        final HttpEntity entity = new HttpEntity(headers);

        return restTemplate.exchange(uriBuilder.toUriString(), HttpMethod.GET, entity, loansParameterizedTypeReference);
    }

}

