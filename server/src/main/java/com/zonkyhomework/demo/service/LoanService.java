package com.zonkyhomework.demo.service;

import com.zonkyhomework.demo.dto.MarketplaceLoan;
import com.zonkyhomework.demo.dto.Rating;
import com.zonkyhomework.demo.dto.converter.LoanConverter;
import com.zonkyhomework.demo.exception.ApiErrorException;
import com.zonkyhomework.demo.persistance.entity.Loan;
import com.zonkyhomework.demo.persistance.repository.LoanRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.Comparator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class LoanService {

    private static final Logger logger = LoggerFactory.getLogger(LoanService.class);

    private static final String GENERAL_ERROR_MESSAGE = "An error occurred during processing the request";

    private final LoanRepository loanRepository;
    private final MarketplaceService marketplaceService;
    private final LoanConverter loanConverter;

    @Autowired
    public LoanService(MarketplaceService marketplaceService,
                       LoanRepository loanRepository, LoanConverter loanConverter) {
        this.loanRepository = loanRepository;
        this.marketplaceService = marketplaceService;
        this.loanConverter = loanConverter;
    }

    /**
     * Fetches all loan records from the Zonky's marketplace and stores them in the database.
     * Also check for loans that are no longer active and delete them from the database
     */
    public void fetchAndStoreLoans() {
        final List<MarketplaceLoan> marketplaceLoans = marketplaceService.fetchAllActiveLoans();

        if (!CollectionUtils.isEmpty(marketplaceLoans)) {
            //get marketplace ids
            final Set<Long> marketPlaceLoansIds = marketplaceLoans
                    .stream()
                    .map(MarketplaceLoan::getId)
                    .collect(Collectors.toSet());

            final List<Loan> loans = loanRepository.findAll();

            //find all stored loans that are no longer active
            final List<Loan> loansToDelete = loans
                    .stream()
                    .filter(l -> !marketPlaceLoansIds.contains(l.getId()))
                    .collect(Collectors.toList());

            if (!loansToDelete.isEmpty()) {
                deleteLoans(loansToDelete);
            }

            saveLoans(marketplaceLoans);
        }
    }

    /**
     * Retrieves all loans from the database
     *
     * @return the collection of loans {@link List<Loan>}
     */
    public List<Loan> getAllLoans() {
        try {
            return loanRepository.findAll();
        } catch (DataAccessException exception) {
            logger.error("An exception occurred during getAllLoans", exception);
            throw new ApiErrorException(HttpStatus.INTERNAL_SERVER_ERROR, GENERAL_ERROR_MESSAGE);
        }
    }

    /**
     * Retrieves set of ratings and their values
     *
     * @return the set of ratings {@link Set<Rating>}
     */
    public Set<Rating> getRatings() {
        try {
            return loanRepository.findAll()
                    .stream()
                    .sorted(Comparator.comparing(Loan::getInterestRate))
                    .map(loan -> new Rating(loan.getRating(), loan.getInterestRate()))
                    .collect(Collectors.toCollection(LinkedHashSet::new));

        } catch (DataAccessException exception) {
            logger.error("An exception occurred during getRatings", exception);
            throw new ApiErrorException(HttpStatus.INTERNAL_SERVER_ERROR, GENERAL_ERROR_MESSAGE);
        }
    }

    /**
     * Retrieves loans based on provided rating
     *
     * @return the collection of loans {@link List<Loan>}
     */
    public List<Loan> getAllByRating(String rating) {
        try {
            return loanRepository.findByRating(rating);
        } catch (DataAccessException exception) {
            logger.error("An exception occurred during getByRating with: {}", rating, exception);
            throw new ApiErrorException(HttpStatus.INTERNAL_SERVER_ERROR, GENERAL_ERROR_MESSAGE);
        }
    }

    private void saveLoans(List<MarketplaceLoan> responseLoans) {
        final List<Loan> loanEntities = responseLoans
                .stream()
                .map(loanConverter::convert)
                .collect(Collectors.toList());
        try {
            loanRepository.saveAll(loanEntities);
        } catch (DataAccessException exception) {
            logger.error("Saving loans to the database failed", exception);
        }
    }

    private void deleteLoans(List<Loan> loansToDelete) {
        try {
            loanRepository.deleteAll(loansToDelete);
        } catch (DataAccessException exception) {
            logger.error("Deleting loans from the database failed", exception);
        }
    }
}
