package com.zonkyhomework.demo.exception;

import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

/**
 * This class handles exceptions of certain types, processes them as response for the client.
 */
@ControllerAdvice
public class CustomExceptionHandler extends ResponseEntityExceptionHandler {

    /**
     * Catching and processing all uncaught exceptions coming from the database.
     */
    @ExceptionHandler(value = DataAccessException.class)
    protected ResponseEntity<ApiError> handleDatabaseError(DataAccessException exception, WebRequest request) {
        final ApiError apiError = new ApiError(HttpStatus.INTERNAL_SERVER_ERROR);
        return createErrorResponse(apiError);
    }

    /**
     * Catching and processing all {@link ApiErrorException} exceptions coming from the application
     */
    @ExceptionHandler(value = ApiErrorException.class)
    protected ResponseEntity<ApiError> handleApiError(ApiErrorException exception, WebRequest request) {
        final ApiError apiError = new ApiError(exception.getStatusCode(), exception.getMessage());
        return createErrorResponse(apiError);
    }

    private ResponseEntity<ApiError> createErrorResponse(ApiError apiError) {
        return new ResponseEntity<>(apiError, apiError.getStatusCode());
    }
}
