package com.zonkyhomework.demo.exception;


import org.springframework.http.HttpStatus;

/**
 * This runtime exception {@link RuntimeException} should be thrown whenever
 * there's an error we want to communicate to the client
 */
public class ApiErrorException extends RuntimeException {

    private HttpStatus statusCode;

    public ApiErrorException(HttpStatus statusCode, String message) {
        super(message);
        this.statusCode = statusCode;
    }

    public ApiErrorException(HttpStatus statusCode, String message, Throwable cause) {
        super(message, cause);
        this.statusCode = statusCode;
    }

    public HttpStatus getStatusCode() {
        return statusCode;
    }
}


