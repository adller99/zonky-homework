package com.zonkyhomework.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class ZonkyHomeworkApplication {

	public static void main(String[] args) {
		SpringApplication.run(ZonkyHomeworkApplication.class, args);
	}
}
