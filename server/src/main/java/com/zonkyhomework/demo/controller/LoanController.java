package com.zonkyhomework.demo.controller;

import com.zonkyhomework.demo.dto.Rating;
import com.zonkyhomework.demo.persistance.entity.Loan;
import com.zonkyhomework.demo.service.LoanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Set;

/**
 * This controller {@link LoanController} handles HTTP calls for the /loans endpoint
 */
@RestController
@RequestMapping("/loans")
public class LoanController {

    private final LoanService loanService;

    @Autowired
    public LoanController(LoanService loanService) {
        this.loanService = loanService;
    }

    /**
     * Accepts requests from /loans of type GET
     * If no data are retrieved 204 status is returned
     * @return collection of {@link Loan} wrapped in a response {@link ResponseEntity} object
     */
    @GetMapping
    public ResponseEntity<List<Loan>> getLoans() {
        final List<Loan> loanList = loanService.getAllLoans();

        if (loanList.isEmpty()) {
            return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity<>(loanList, HttpStatus.OK);
    }

    /**
     * Accepts requests from /loans/ratings of type GET
     * If no data are retrieved 204 status is returned
     * @return set of {@link Loan} wrapped in a response {@link ResponseEntity} object
     */
    @GetMapping("/ratings")
    public ResponseEntity<Set<Rating>> getRatings() {
        final Set<Rating> ratings = loanService.getRatings();

        if (ratings.isEmpty()) {
            return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity<>(ratings, HttpStatus.OK);
    }

    /**
     * Accepts requests from /loans/{rating} of type GET
     * If no data are retrieved 204 status is returned
     * @param rating the filtering parameter
     * @return collection of loans {@link Loan} filtered by the provided rating,
     * wrapped in a response {@link ResponseEntity} object
     */
    @GetMapping("/{rating}")
    public ResponseEntity<List<Loan>> getAllByRating(@PathVariable("rating") String rating) {
        final List<Loan> loanEntities = loanService.getAllByRating(rating.toUpperCase());

        if (loanEntities.isEmpty()) {
            return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity<>(loanEntities, HttpStatus.OK);
    }
}
